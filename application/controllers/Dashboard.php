<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  public $message = '';
  public $status = TRUE;
  public $data = [];

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('DashboardModel', 'dashboard');
  }

  public function index()
  {
    $this->load->view('dashboard/index');
  }

  /* 
   * funciton get all data
   */
  public function getData()
  {
    if($this->input->method() == 'post') {
      $data = $this->dashboard->getData();
      if(isset($data) && !empty($data))
      {
        $this->status = TRUE;
        $this->message = "Lây dữ liệu thành công";
        $this->data = $data;
      }
    }
    $this->_response();
  }

  /* 
   * funciton get data by ID
   */
  public function getDataByID()
  {
    if($this->input->method() == 'post') {
      $user_id = $this->input->post('user_id');
      $user = $this->dashboard->getDataByID($user_id);
      if (isset($user) && !empty($user)) {
        $this->status = TRUE;
        $this->message = "Lây dữ liệu thành công";
        $this->data = $user;
      }
    }
    $this->_response();
  }

  /* 
   * funciton insert data
   */
  public function insertData()
  {
    if($this->input->method() == 'post') {
      $this->_validate();
      if ($this->status == TRUE)
      {
        $data = [
          'first_name' => $this->input->post('first_name'),
          'last_name' => $this->input->post('last_name'),
          'gender' => $this->input->post('gender'),
          'address' => $this->input->post('address'),
          'date_of_birth' => $this->input->post('date_of_birth'),
        ];
  
        if(!empty($_FILES['image']['name'])) {
          $upload = $this->_upload_image();
          $data['img_name'] = $upload;
        }
        $this->status = TRUE;
        $this->message = "Thêm dữ liệu thành công";
        $this->data = $data;
        $insert = $this->dashboard->insertData($this->data);
      }      
    }
    $this->_response();
  }

  /* 
   * funciton update data
   */
  public function updateData()
  {
    if($this->input->method() == 'post') {
      $this->_validate();
      if ($this->status == TRUE)
      {
        $data = [
          'first_name' => $this->input->post('first_name'),
          'last_name' => $this->input->post('last_name'),
          'gender' => $this->input->post('gender'),
          'address' => $this->input->post('address'),
          'date_of_birth' => $this->input->post('date_of_birth'),
        ];

        $user = $this->dashboard->getDataByID($this->input->post('user_id'));
        
        if ($this->input->post('remove_img') && is_file('uploads/'.$this->input->post('remove_img'))) {
          unlink('uploads/' . $this->input->post('remove_img'));
          $data["img_name"] = '';
        }
  
        if (!empty($_FILES['image']['name'])) {
          $upload = $this->_upload_image();
          $data["img_name"] = $upload;
        }
        $is_update = $this->dashboard->updateData(array('user_id' => $this->input->post('user_id')), $data);
        if ($is_update == 1)
        {
          $this->status = TRUE;
          $this->message = "Update thành công";
          $this->data = $data;
          if ($user->img_name !== '' && is_file('uploads/' . $user->img_name)) {
            unlink('uploads/' . $user->img_name);
          }
        } 
        else
        {
          $this->status = FALSE;
          $this->message = "Update thất bại";
          $this->data = [];
        }
      }
    }
    $this->_response();
  }

  /* 
   * funciton delete data
   */
  public function deleteData()
  {
    if($this->input->method() == 'post') {
      $user_id = $this->input->post('user_id');
      $user = $this->dashboard->getDataByID($user_id);
      $result = $this->dashboard->deleteData($user_id);
      if ($result == 1)
      {
        if (file_exists('uploads/' . $user->img_name) && $user->img_name)
        {
          unlink('uploads/' . $user->img_name);
        }
        $this->status = TRUE;
        $this->message = "Xóa thành công";
      }
      else
      {
        $this->status = FALSE;
        $this->message = "Xóa thất bại";
      }
    }
    $this->_response();
  }

  /* 
   * funciton response
   */
  private function _response()
  {
    echo json_encode(
      [
        'status' => $this->status,
        'message' => $this->message,
        'data' => $this->data
      ],
      JSON_UNESCAPED_UNICODE
    );
  }

  /* 
   * funciton upload image
   * return file name
   */
  private function _upload_image()
  {
    $config['upload_path'] = 'uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['file_name'] = round(microtime(true) * 1000);
    
    $this->load->library('upload', $config);
    
    if (!$this->upload->do_upload('image')){
      $this->status = FALSE;
      $this->message = $this->upload->display_errors();
      $this->data = ["image" => "File không hợp lệ"];
      $this->_response();
      exit();
    }
    return $this->upload->data('file_name');
  }

  /* 
   * funciton validate form
   */
  private function _validate()
  {
    $rules = [
      [
        'field' => 'first_name',
        'label' => 'First name',
        'rules' => 'trim|required',
      ],[
        'field' => 'last_name',
        'label' => 'Last name',
        'rules' => 'trim|required',
      ],[
        'field' => 'gender',
        'label' => 'Gender',
        'rules' => 'trim|required',
      ],[
        'field' => 'address',
        'label' => 'Address',
        'rules' => 'trim|required',
      ],[
        'field' => 'date_of_birth',
        'label' => 'Date of birth',
        'rules' => 'trim|required',
      ]
    ];
    $this->form_validation->set_rules($rules);
    if ($this->form_validation->run() == false)
    {
      $this->status = FALSE;
      $this->message = "Vui lòng kiểm tra lại dữ liệu vừa nhập";
      if (!empty($rules))
      {
        foreach ($rules as $item)
        {
          if(!empty(form_error($item['field']))) $this->data[$item['field']] = $item['label'] . ' không được bỏ trống';
        }
      }
    }
  }
}
