<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model {
  private $table = "user";

  public function __construct()
  {
    parent::__construct();
  }

  public function getData()
  {
    $this->db->select('*');
    $query = $this->db->get($this->table);
    return $query->result();
  }

  public function getDataByID($id)
  {
    $this->db->from($this->table);
    $this->db->where('user_id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function insertData($data = [])
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function updateData($where, $data = [])
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function deleteData($id)
  {
    $this->db->where('user_id', $id);
    $this->db->delete($this->table);
    return $this->db->affected_rows();
  }
}
