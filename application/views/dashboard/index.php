<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard</title>
  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>public/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>public/fontawesome/css/fontawesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>public/datepicker/css/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>public/dashboard/css/style.css">
</head>
<body>
  <header class="header">
    <div class="container">
      <h1 class="header-title">Dashboard</h1>
    </div>
  </header><!-- /header -->
  
  <div class="user">
    <div class="container-fluid">
      <div class="container">
        <div class="user-wrapper">
          <div class="user-header">
            <h2 class="user-header__ttl">Users List</h2>
            <div class="user-header-create">
              <button class="btn btn-primary user-header-create__btn js-insertUser" data-toggle="modal" data-target="#modal_form"><i class="fas fa-plus"></i>add user</button>
            </div>
          </div><!-- /user-header -->
          <div class="user-content">
            <table class="user-table table table-striped table-bordered">
              <colgroup>
                <col style="width: 150px">
                <col style="width: 150px">
                <col style="width: 150px">
                <col style="width: 150px">
                <col style="width: 150px">
                <col style="width: 150px">
                <col style="width: 150px">
              </colgroup>
              <thead>
                <tr>
                  <th class="text-center">First Name</th>
                  <th class="text-center">Last Name</th>
                  <th class="text-center">Gender</th>
                  <th class="text-center">Address</th>
                  <th class="text-center">Date of Birth</th>
                  <th class="text-center">Image</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody id="tbody"></tbody>
            </table>
          </div><!-- /user-content -->
        </div><!-- /user-wrapper -->
      </div>
    </div>
  </div><!-- /user -->

  <?php $this->load->view('dashboard/modal.inc'); ?>

  <script src="<?= base_url() ?>public/jquery/jquery.js" defer></script>
  <script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js" defer></script>
  <script src="<?= base_url() ?>public/datepicker/js/bootstrap-datepicker.min.js" defer></script>
  <script src="<?= base_url() ?>public/dashboard/js/script.js" defer></script>
  
</body>
</html>