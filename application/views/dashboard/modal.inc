<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title js-modalTitle">Person Form</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body form">
        <form action="" id="form" class="form-horizontal">
          <input type="hidden" value="" name="user_id"/> 
          <div class="form-body">
            <div class="form-group">
              <label class="control-label font-weight-bold">First Name <sup class="text-danger">*</sup></label>
              <input name="first_name" placeholder="First Name" class="form-control" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold">Last Name <sup class="text-danger">*</sup></label>
              <input name="last_name" placeholder="Last Name" class="form-control" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold">Gender <sup class="text-danger">*</sup></label>
              <select name="gender" class="form-control">
                <option value="">--Select Gender--</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold">Address <sup class="text-danger">*</sup></label>
              <textarea name="address" placeholder="Address" class="form-control"></textarea>
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold">Date of Birth <sup class="text-danger">*</sup></label>
              <input name="date_of_birth" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group js-imgPreview">
              <label class="control-label font-weight-bold">Photo</label>
              <div class="js-imgStatus">(No photo)</div>
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold" id="imgLabel">Upload image</label>
              <input name="image" type="file">
              <div class="form-alert js-alert text-danger"></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary js-btnSave">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->