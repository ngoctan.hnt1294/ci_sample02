let submitMethod;
let form = $("#form");
/*
  * Function insert user
  **/
function insertUser() {
  submitMethod = "insert";
  $(".js-modalTitle").text("Add user");
  $(".js-imgPreview").hide();
  $(".js-alert").empty();
  form[0].reset();
}

/*
  * Function insert user
  **/
function updateUser(current) {
  let user_id = $(current).parent("td").find("#user_id").val();
  submitMethod = "update";
  form[0].reset();
  $(".js-alert").empty();
  $(".js-modalTitle").text("Update user");
  let updateAjax = $.ajax({
    type: "POST",
    url: "/Dashboard/getDataByID",
    data: {
      user_id: user_id
    }
  });

  updateAjax.done(function(response) {
    checkResponse(response);
    let responseList = JSON.parse(response);
    let status = responseList.status;
    let data = responseList.data;
    if (status == true) 
    {
      checkData(data);
      $('[name="user_id"]').val(data["user_id"]);
      $('[name="first_name"]').val(data["first_name"]);
      $('[name="last_name"]').val(data["last_name"]);
      $('[name="gender"]').val(data["gender"]);
      $('[name="address"]').val(data["address"]);
      $('[name="date_of_birth"]').val(data["date_of_birth"]);
      $(".js-imgPreview").show();
      if(data.img_name) {
        $("#imgLabel").text("Change image");
        $(".js-imgStatus").addClass("img-show").html("<figure><img src=\"/uploads/" + data.img_name + "\" alt=\"\" class=\"img-responsive\"></figure>");
        $(".js-imgStatus").append("<label><input type=\"checkbox\" name=\"remove_img\" value=\"" + data.img_name + "\"/>Remove photo when saving</label>");
      } else {
        $("#imgLabel").text("Upload image");
        $(".js-imgStatus").text("(No image)");
      }
    }
    else 
    {
      console.log("Có lỗi phát sinh");
    }
    
  });

  updateAjax.fail(function(jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ': ' + errorThrown);
  });
}

/*
  * Function delete user
  **/
function deleteUser(current) {
  let user_id = $(current).parent("td").find("#user_id").val();
  if (confirm("Are you sure to delete this user???")) {
    let deleteAjax = $.ajax({
      type: "POST",
      url: "/Dashboard/deleteData",
      data: {
        user_id: user_id
      }
    });
    deleteAjax.done(function(response) {
      checkResponse(response);
      let responseList = JSON.parse(response);
      let status = responseList.status;
      if (status == true)
      {
        showAllUser();
      }
      else
      {
        console.log("Có lỗi phát sinh");
      }
    });
    deleteAjax.fail(function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus + ': ' + errorThrown);
    });
  } else {
    return false;
  }
}

/*
  * Function show all user
  **/
function showAllUser() {
  let ajaxShow = $.ajax({
    type: "POST",
    url: "/Dashboard/getData",
  });
  ajaxShow.done(function(response) {
    checkResponse(response);
    let responseList = JSON.parse(response);
    let status = responseList.status;
    let data = responseList.data;
    if (status == true)
    {
      checkData(data);
      console.log(data);
      manageRow(data);
    }
    else 
    {
      console.log("Có lỗi phát sinh");
    }
  });
  ajaxShow.fail(function(jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ': ' + errorThrown);
  });
}

/*
  * Function submit form
  **/
function submitDataUser() {
  let formData = new FormData(form[0]);
  let url = '';
  $('.js-btnSave').attr('disabled',true);
  if(submitMethod == "insert") {
    url = "/Dashboard/insertData";
  } else if (submitMethod == "update") {
    url = "/Dashboard/updateData";
  }
  let ajaxCall = $.ajax({
    type: "POST",
    url: url,
    data: formData,
    processData: false,
    contentType: false,
  });

  ajaxCall.done(function (response) {
    checkResponse(response);
    let responseList = JSON.parse(response);
    let status = responseList.status;
    let data = responseList.data;
    $('.js-btnSave').attr('disabled',false);
    if (status == true) 
    {
      $("#modal_form").modal("hide");
      showAllUser();
    }
    else 
    {
      checkData(data);
      for (let [key, value] of Object.entries(data)) 
      {
        $('[name="'+ key +'"]').parent(".form-group").find(".js-alert").html(value);
      }
    }
  });

  ajaxCall.fail(function (jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ": " + errorThrown);
  });
}

function checkResponse(response)
{
  if (response == '' || response == undefined)
  {
    console.log("Không có response trả về");
    return;
  }
}

function checkData(data)
{
  if (data == undefined || data.length == 0) 
  {
    console.log("Không có data hoặc data không hợp lệ");
    return;
  }
}

/*
  * Function manage row
  **/
function manageRow(data) {
  let row = [];
  
  $.each(data, function (index, value) {
      let imgDir = value.img_name !== '' ? "/uploads/" + value.img_name  : '';
      let imgCell = '';
      if (value.img_name !== '') {
        imgCell = 
          "<figure class=\"user-table__img\">" + 
            "<img src=\"" + imgDir + "\" alt=\"\" class=\"img-responsive\">" + 
          "</figure>";
      } else {
        imgCell = "(No Image)";
      }
      let htmlCode = 
      "<tr>" +
        "<td class=\"text-center\">" + value.first_name + "</td>" +
        "<td class=\"text-center\">" + value.last_name + "</td>" +
        "<td class=\"text-center\">" + value.gender + "</td>" +
        "<td class=\"text-center\">" + value.address + "</td>" +
        "<td class=\"text-center\">" + value.date_of_birth + "</td>" +
        "<td class=\"text-center\">" + imgCell +"</td>" +
        "<td class=\"text-center\">" +
          "<input type=\"hidden\" id=\"user_id\" name=\"user_id\" value=\"" + value.user_id + "\">" +
          "<a class=\"btn btn-sm btn-primary js-edit\" href=\"javascript:void(0)\" title=\"Edit\" data-toggle=\"modal\" data-target=\"#modal_form\"><i class=\"glyphicon glyphicon-pencil\"></i> Edit</a>" +
          "<a class=\"btn btn-sm btn-danger js-delete\" href=\"javascript:void(0)\" title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> Delete</a>" +
        "</td>" +
      "</tr>";
      row.push(htmlCode);
  });
  $("#tbody").html(row);
}

$(function () {
  showAllUser();
  $(document).on("click", ".js-insertUser", function (event) {
    event.preventDefault();
    insertUser();
  });

  $(document).on("click", ".js-btnSave", function (event) {
    event.preventDefault();
    submitDataUser();
  });
  
  $(document).on("click", ".js-delete", function (event) {
    event.preventDefault();
    deleteUser(this);
  });

  $(document).on("click", ".js-edit", function (event) {
    event.preventDefault();
    updateUser(this);
  });

  $(document).on("change", "input", function () {
    $(this).parent(".form-group").find(".js-alert").empty();
  });

  $(document).on("change", "textarea", function () {
    $(this).parent(".form-group").find(".js-alert").empty();
  });

  $(document).on("change", "select", function () {
    $(this).parent(".form-group").find(".js-alert").empty();
  });

  //datepicker
  $(".datepicker").datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
  });
});
